# ecm_input_forms

ECM input forms designed for utilities and energy management stewards at UT to be able to enter data into the postgres database.

## Link
[Link to deployed app](https://ecmprojectform.com/login/)

## Installation

- git clone the repo.
- cd into root folder.
- Use the package manager npm to install ecm_input_forms
```bash
npm install
```

## Built With
- JavaScript
- jQuery
- Handlebars
- HTML
- CSS
- Node
- Sequelize
- Postgres
- Bootstrap
